# M42 Mount

| header | flangeback(mm) |Inner diameter(mm)|
| ------ | ------ |------|
| M42 | 45.46 |42.00|
| Nikon F | 47.00 |46.50|
|Pentax K|45.46|48.00|
|Canon EF|44.00|54.00|

- M42レンズをNikon Fマウントに装着した場合、フランジバックの距離はM42本来の規格より長くなる. <br>レンズを無限に合わせても手前までしかピントが来ないという状態になる. <br>なので、無限遠補正レンズ付きの変換マウンタが必要になる.

## M42レンズの種類
- M42マウントで策定されているのはFlangeback値と内径だけで、実際には様々な種類がある.

- 自動絞りピンが有る
  - Auto/Manual切替あり : Takumar, ZeissJenaの後期
  - Preset絞り : Zeis Jenaのゼブラ柄(1970年代)

- 自動絞りピンが無い
  - 実絞り : 1950-60年代に多い
  - Preset絞り : Helios44, Helios44-2, INDUSTAR-6IL/Z 50mm, Mir-1b 35mm

*自動絞りピン：ピンを押し込むことで、絞りが開閉する。絞り測光ではピント合わせに難があり、開放測光の為に追加された。しかし、押込み長さは各社まちまちである。

## マウントアダプター
- K&F : 絞り連動ピン押込みタイプ. ただし、押込み部を取り外し可能.
- Rayqual : 絞り連動ピン押込みタイプ. 押込み部取り外し不可.



# weight
| name                      | weight(g)       |
|---------------------------|-----------------|
| Foca Pf3                  | 481             |
| Praktika IV               | 700             |
| XE-4                      | 364(314)        |
| fuji x to L39 adapter     | 46              |
| fuji x to M adapter       | 80?             |
| fuji x to M42 adapter     | 120-160         |
| alpha7 III                | 650(565)        |
| Pentax k-3 MarkIII        | 820(735)        |
| Pentax K-7                | 768             |
| Leica M2                  | 560             |
| Leica M3                  | 595             |
| Leica M4                  | 600             |
| Leica M5                  | 680             |
| Leica M6                  | 560             |
| Leica IIIa                | 410             |
| Leica IIIb                | 428             |
| oplar 50mm f2.8           | 79 + 14(filter) |
| CarlZeissJena Teaasr 50mm | 113             |
| Mir-1B 37mm               | 183             |
| Nikkor-h.c 50mm f2        | 218             |

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:7e6dd5d51b60ecb98a0fe708c8ef09be?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:7e6dd5d51b60ecb98a0fe708c8ef09be?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:7e6dd5d51b60ecb98a0fe708c8ef09be?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/hide-sato/m42lens.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:7e6dd5d51b60ecb98a0fe708c8ef09be?https://gitlab.com/hide-sato/m42lens/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:7e6dd5d51b60ecb98a0fe708c8ef09be?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:7e6dd5d51b60ecb98a0fe708c8ef09be?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:7e6dd5d51b60ecb98a0fe708c8ef09be?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://gitlab.com/-/experiment/new_project_readme_content:7e6dd5d51b60ecb98a0fe708c8ef09be?https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:7e6dd5d51b60ecb98a0fe708c8ef09be?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:7e6dd5d51b60ecb98a0fe708c8ef09be?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:7e6dd5d51b60ecb98a0fe708c8ef09be?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:7e6dd5d51b60ecb98a0fe708c8ef09be?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:7e6dd5d51b60ecb98a0fe708c8ef09be?https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://gitlab.com/-/experiment/new_project_readme_content:7e6dd5d51b60ecb98a0fe708c8ef09be?https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:7e6dd5d51b60ecb98a0fe708c8ef09be?https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.


