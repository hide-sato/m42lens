
import rawpy
import imageio

path = '_IGP3088.DNG'

# Load a RAW file and save the postprocessed image using default parameters:
# with rawpy.imread(path) as raw:
#     rgb = raw.postprocess()
# imageio.imsave('default.tiff', rgb)

# Save as 16-bit linear image:
with rawpy.imread(path) as raw:
    rgb = raw.postprocess(gamma=(1, 1), no_auto_bright=True, output_bps=16, output_color=rawpy.ColorSpace.Wide, half_size=True)
imageio.imsave('linear_Wide.tiff', rgb)

# rawpy.ColorSpace(1)
#
# Adobe= 2
# ProPhoto= 4
# Wide= 3
# XYZ= 5
# raw= 0
# sRGB= 1